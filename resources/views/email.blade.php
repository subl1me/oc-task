<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Notification Email</title>
</head>
<body>
    <h1>Notification for new Colleague</h1>
    <p>Name: {{ $employee['name'] }}</p>
    <p>email: {{ $employee['email'] }}</p>
</body>
</html>