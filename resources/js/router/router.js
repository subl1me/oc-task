import {createRouter, createWebHistory} from 'vue-router'

// public routes
const Login = () => import('@/components/Login.vue')
const Register = () => import('@/components/Register.vue')

//authenticated routes
const AuthenticatedLayout = () => import('@/components/layouts/Default.vue')
const Dashboard = () => import('@/components/Dashboard.vue')
const Account = () => import('@/components/Account.vue')

const routes = [
    {
        name: "login",
        path: "/login",
        component: Login,
        meta: {
            requiresAuth: false,
            title: `Login`
        }
    },
    {
        name: "register",
        path: "/register",
        component: Register,
        meta: {
            requiresAuth: false,
            title: `Register`
        }
    },
    {
        path: "/",
        component: AuthenticatedLayout,
        meta: {
            requiresAuth: true,
        },
        children: [
            {
                name: "dashboard",
                path: '/',
                component: Dashboard,
                meta: {
                    requiresAuth: true,
                    title: `Dashboard`
                }
            },
            {
                name: "account",
                path: "/account",
                component: Account,
                meta: {
                    requiresAuth: true,
                    title: 'Account Details'
                }
            }
        ]
    },
    {
        // custom regex to get non-existing pages
        path: '/:catchAll(.*)',
        name: 'NonExisting',
        redirect: '/'
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

function isLoggedIn() {
    return localStorage.getItem("token")
}

axios.interceptors.request.use(function (config) {
    const token = localStorage.getItem("token");
    if (token) {
        config.headers.Authorization = token;
    }
    return config;
});

// TODO: better routing to check auth token existence / expiration
router.beforeEach((to) => {
    const token = localStorage.getItem("token")
    if (token) {
        axios.post('/api/check-auth', {token: token}).then(({data}) => {
            if (data.route) {
                localStorage.removeItem('token')
                this.$router.push('/')
            }
        }).catch((response) => {
            console.log(response)
        })
    }
    // instead of having to check every route record with
    // to.matched.some(record => record.meta.requiresAuth)
    if (to.meta.requiresAuth && !isLoggedIn()) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        return {
            path: '/login',
            // save the location we were at to come back later
            query: {redirect: to.fullPath},
        }
    }
})
export default router