import './bootstrap';
import '../sass/app.scss'
import { createApp } from 'vue';

import Router from '@/router/router'


const app = createApp({});

app.config.globalProperties.$log = console.log

app.use(Router)

app.mount('#app');
