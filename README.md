<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

`PHP ^8.0` &nbsp; | &nbsp;  `Laravel v9.36` &nbsp;  | &nbsp; `Node ^16.14`


## Setup `.env` file
find `SESSION_DRIVER=file` and change to `SESSION_DRIVER=cookie`
#### at the end of the file add

```sh
QUEUE_DRIVER=database
SESSION_DOMAIN=localhost
SESSION_SECURE_COOKIE=false
SANCTUM_STATEFUL_DOMAINS=localhost,localhost:8000,127.0.0.1,127.0.0.1:8000,::1
```

## Setup project
Open terminal and run:

```sh
composer install
npm install
```

Check `.env` file and if `APP_KEY` is empty run command:
```sh
php artisan key:generate
```

Open `database/seeders/DatabseSeeder.php` and edit how many employees would like to add

line 27 `$employees = User::factory(20)->create(['role_id' => 2]);`

for this example will add 20 users



### Artisan commands:

```sh
php artisan migrate --seed
```

### Download
Download [Default Image](https://plovdevs.com/default.jpg) and put it in folder `storage/app/public/uploads/`

After that run command
```sh
php artisan storage:link
```

Open 2 terminals and run
```sh
php artisan serve
npm run dev
```
if you have problems edit `.env` file to match `vite port ` and `artisan server port`

`APP_URL=http://localhost:8000`


### Login Credentials - Managers
email
```sh
admin@admin.com
admin2@admin.com
admin3@admin.com
admin4@admin.com
admin5@admin.com
```
password for managers accounts
```sh
123123123
```
