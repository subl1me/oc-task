<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManagerEmployee extends Model {

    use HasFactory;

    protected $table = 'managers_employees';
    protected $guarded = [];

    //TODO: Edit this to single relation if can
    public function managers()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }
    public function employees()
    {
        return $this->belongsTo(User::class, 'employee_id', 'id');
    }
}
