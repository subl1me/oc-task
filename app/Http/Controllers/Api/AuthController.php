<?php

namespace App\Http\Controllers\Api;

use App\Http\Actions\RegisterEmployeeAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\PersonalAccessToken;

class AuthController extends Controller {

    public function login(LoginUserRequest $request)
    {
        // LoginUserRequest custom validator validating data
        $request->validated($request->all());

        if ( ! Auth::attempt($request->only(['email', 'password'])))
        {
            return response()->json([
                'message' => 'Credentials do not match'
            ], 401);
        }

        $user = User::where('email', $request->email)->first();

        return response()->json([
            'token' => $user->createToken('Api Token of ' . $user->name, expiresAt: Carbon::now()->addHours(5))->plainTextToken
        ], 200);
    }

    //register user with custom request and registration handling
    public function register(StoreUserRequest $request, RegisterEmployeeAction $registerAction)
    {
        // handle registration with custom Action to keep the controller clean
        $user = $registerAction->handler($request);

        return response()->json([
            'token' => $user->createToken('Api Token of ' . $user->name, expiresAt: Carbon::now()->addHours(5))->plainTextToken
        ], 200);
    }

    public function logout()
    {
        //delete all authentication tokens for current user.
        // TODO: Need different approach to delete current token not all of them.
        auth()->user()->tokens()->delete();

        return response()->json([
            'message' => 'Logged out ? I hope we\'ll see you soon again! ',
        ], 200);
    }

    public function authCheck(Request $request)
    {
        $date = new Carbon();
        $token = PersonalAccessToken::findToken($request->get('token'));
        $route = false;
        //check token expiration
        if (isset($token) && $date > $token->expires_at)
        {
            $route = true;
        }

        return response()->json([
            'route' => $route,
        ], 200);
    }

}
