<?php

namespace App\Http\Controllers\Api;

use App\Http\Actions\UserInfoAction;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\ImageFile;
use Illuminate\Validation\Rules\Password;

class UserManagerController extends Controller {

    public function index(UserInfoAction $action)
    {
        // TODO: can use Resources transformation layer (model data to json format) to handle return as json (obj with data and relation is passing instead). auth()->user() is not best way.
        // UserInfoAction - is like resources class
        return response()->json([
            'user' => $action->handler(auth()->user())
        ]);
    }

    // Can be done with Response class, which provides a variety of methods for building HTTP responses
    public function update(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'name'      => ['sometimes', 'string', 'max:255'],
            'email'     => ['sometimes', 'string', 'max:255', Rule::unique('users')->ignore($user->id)],
            'password'  => ['sometimes', 'confirmed', Password::default()],
            'userImage' => ['sometimes', ImageFile::default()]
        ]);

        try
        {
            // null coalescing operator to check request inputs existence
            $user->name = $request->get("name") ?? $user->name;
            $user->email = $request->get("email") ?? $user->email;
            $user->password = $request->get("password") ? bcrypt($request->get("password")) : $user->password;
            //check for selected new image
            if ($request->file("userImage"))
            {
                // Deleting current user image before store new one ( new image pass validation )
                if (\Illuminate\Support\Facades\File::exists(public_path() . $user->image_path))
                {
                    \Illuminate\Support\Facades\File::delete(public_path() . $user->image_path);
                }

                $file_name = uniqid('', true) . '.' . $request->file('userImage')->extension();
                $file_path = $request->file('userImage')->storePubliclyAs('uploads', $file_name, 'public');

                $user->image_name = $file_name;
                $user->image_path = '/storage/' . $file_path;
            }

            $user->update();
        } catch (Exception $exception)
        {
            return $exception->getMessage();
        }

        return response()->json([
            'user' => $user,
            'role' => $user->role_id
        ], 200);
    }

}
