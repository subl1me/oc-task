<?php

namespace App\Http\Controllers\Api;

use App\Http\Actions\DashboardInitAction;
use App\Http\Controllers\Controller;
use App\Models\ManagerEmployee;

class DashboardController extends Controller {

    public function index(DashboardInitAction $action)
    {
        // this is not correct approach but for the sake of tests and learning im using available auth helper instance
        $user = auth()->user();

        // passing all employees of current Manager
        if ($user->role_id === 1)
        {
            // getting all employees of current Manager
            $managers = ManagerEmployee::where('manager_id', '=', $user->id)->with('employees')->get();
            // using custom Action handler to keep the controller clean
            $employees = $action->handler($managers, $user->role_id);

            return response()->json([
                'users' => $employees,
                'role' => $user->role_id,
                'user_image' => $user->image_path,
                'name' => $user->name
            ]);
        }
        //TODO: need optimization for querying Managers / Employees

        // getting all managers of current employee
        $employees = ManagerEmployee::where('employee_id', '=', $user->id)->with('managers')->get();
        // using custom Action handler to keep the controller clean
        $managers = $action->handler($employees, $user->role_id);

        return response()->json([
            'users' => $managers,
            'role' => $user->role_id,
            'user_image' => $user->image_path,
            'name' => $user->name
        ]);

    }
}
