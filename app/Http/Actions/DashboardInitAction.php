<?php

namespace App\Http\Actions;

class DashboardInitAction {

    public function handler($users, $role_id)
    {
        $getRelation = $role_id === 1 ? 'employees' : 'managers';
        $usersCollection = [];

        foreach ($users as $relations)
        {
            $usersCollection[] = [
                'id'    => $relations->$getRelation->id,
                'name'  => $relations->$getRelation->name,
                'email' => $relations->$getRelation->email,
                'image_path' => $relations->$getRelation->image_path
            ];
        }
        return $usersCollection;
    }
}