<?php

namespace App\Http\Actions;

class UserInfoAction {

    public function handler($user)
    {
        return [
            'id'    => $user->id,
            'name'  => $user->name,
            'email' => $user->email,
            'image_path' => $user->image_path
        ];
    }
}