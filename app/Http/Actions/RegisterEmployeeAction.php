<?php

namespace App\Http\Actions;

use App\Http\Requests\StoreUserRequest;
use App\Jobs\NotificationMailQueue;
use App\Models\ManagerEmployee;
use App\Models\User;
use DB;
use Exception;
use Illuminate\Support\Carbon;

class RegisterEmployeeAction {

    public function handler(StoreUserRequest $request): User|string
    {
        $request->validated($request->all());

        $managersIds = ManagerEmployee::select([
            'manager_id',
            DB::raw('count(manager_id) as total'),
            DB::raw('MAX(created_at) as latest_added')
        ])
            ->orderBy('latest_added')
            ->orderBy('total')
            ->groupBy('manager_id')
            ->limit(2)
            ->pluck('manager_id')
            ->toArray();

        try
        {
            $user = new User();
            $user->role_id = 2;
            $user->name = $request->get("name");
            $user->email = $request->get("email");
            $user->password = bcrypt($request->get("password"));
            if ($request->file("userImage"))
            {
                $file_name = uniqid() . '.' . $request->file('userImage')->extension();
                $file_path = $request->file('userImage')->storePubliclyAs('uploads', $file_name, 'public');

                $user->image_name = $file_name;
                $user->image_path = '/storage/' . $file_path;
            }
            $user->save();

            \Illuminate\Support\Facades\Log::info(['New User was created with ID: ' => $user->id]);
        } catch (Exception $exception)
        {
            return $exception->getMessage();
        }

        foreach ($managersIds as $managerId)
        {
            try
            {
                ManagerEmployee::create([
                    'manager_id'  => $managerId,
                    'employee_id' => $user->id,
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now(),
                ]);
                \Illuminate\Support\Facades\Log::info([
                    'Employee( ' . $user->name . ' with ID: ' . $user->id . ' ) was assigned ot manager with ID' => $managerId
                ]);
            } catch (Exception $exception)
            {
                return $exception->getMessage();
            }
        }

        $this->notifyEmployees($managersIds, $user);

        return $user;
    }

    protected function notifyEmployees($managersIds, $newUser)
    {
        $managerEmployees = [];
        $notifyEmployees = [];

        foreach ($managersIds as $managerId)
        {
            // array with employees that have to send email for new colleague by assigned managers
            $managerEmployees[] = ManagerEmployee::where([
                ['manager_id', '=', $managerId],
                ['employee_id', '!=', $newUser->id]
            ])->with('employees')->get();

            // TODO fix nested
            foreach ($managerEmployees as $employees)
            {
                foreach ($employees as $employee)
                {
                    $notifyEmployees[] = [
                        'id'    => $employee->employees->id,
                        'name'  => $employee->employees->name,
                        'email' => $employee->employees->email
                    ];
                }
            }
        }
        // filter duplicate emails to prevent sending same email multiple times.
        $notifyEmployees = array_map("unserialize", array_unique(array_map("serialize", $notifyEmployees)));

        foreach ($notifyEmployees as $notifyEmployee)
        {
            //send emails is commented due to missing config of mail server.
//            $sendMail = new NotificationMailQueue((array)$notifyEmployee);
//            dispatch($sendMail);

            \Illuminate\Support\Facades\Log::info([
                'Mail notification was send successfully to' => $notifyEmployee['email']
            ]);
        }
    }
}