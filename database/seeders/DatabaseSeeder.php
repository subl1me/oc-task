<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\ManagerEmployee;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //set roles [manger, employee]
        Role::factory(1)->create(['name' => 'manager']);
        Role::factory(1)->create(['name' => 'employee']);
        // create first 5 users as manager
        for ($num = 1; $$num <= 5; $num++)
        {
            User::factory()->create([
                'role_id'  => 1,
                'email'    => 'admin' . ($num > 1 ? $num : '') . '@admin.com',
                'password' => '$2y$10$aUdntMJZ1C/ma3gZoUeEq.3RzXm3fJcFAQPqFBG1TZ9DeIYbf1pMC'
            ]);
        }
        //create 5000+ employees
        $employees = User::factory(5000)->create(['role_id' => 2]);
        //iterate through all employees to set minimum 2 managers per employee
        $this->initEmployeesToManagers($employees);
    }

    protected function initEmployeesToManagers($employees): void
    {
        //variable to iterate through all managers
        $managerId = 1;

        foreach ($employees as $employee)
        {
            \Illuminate\Support\Facades\Log::info([
                $managerId
            ]);

            ManagerEmployee::factory()->create([
                'manager_id'  => $managerId,
                'employee_id' => $employee->id
            ]);
            $managerId >= 5 ? $managerId = 1 : $managerId++;
            ManagerEmployee::factory()->create([
                'manager_id'  => $managerId,
                'employee_id' => $employee->id
            ]);
            $managerId >= 5 ? $managerId = 1 : $managerId++;
        }
    }
}
